"use strict"
// const { log } = require('console')
require("./App");

const { ipcMain,BrowserWindow} = require("electron");

const electronReload = require('electron-reload');
electronReload(__dirname);

const { download } = require('electron-dl')


let mainWindow;

const getMyFriends = () => {
	return [
		{id: 6030, name: "Twilley"},
		{id: 967, name: "Wickkiser"},
		{id: 5073, name: "Essick"},
		{id: 8886, name: "Marotta"},
		{id: 7416, name: "Banh"}
	];
}

const sendMessage = (message) => {
	console.log(message);
}

ipcMain.on("download", async (event, { payload }) => {

	console.log("ipcMain. download", payload )

    const win = BrowserWindow.getFocusedWindow();

    const properties = { ...payload.properties, onProgress: (progress) => { win.webContents.send("download-progress", progress) }, onCompleted: (item) => { win.webContents.send("download-complete", item) } }

    download(win, payload.url, properties)


})



ipcMain
	.on("main-window-ready", (e) => {
		console.log("Main window is ready");
	})
	.on("send-message", (e, message) => {
		sendMessage(message);
	})

ipcMain.handle("get-friends", (e) => {
	return getMyFriends();
})