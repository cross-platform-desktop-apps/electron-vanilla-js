const { contextBridge, ipcRenderer } = require("electron");

contextBridge.exposeInMainWorld("api", {
	sendMessage: () => {
		const chatBox = document.getElementById("chat-box");
		ipcRenderer.send("send-message", chatBox.value);
		chatBox.value = "";
	},
	getFriends: () => {
		return ipcRenderer.invoke("get-friends");
	},

	
    send: (channel, data) => {

        ipcRenderer.send(channel, data);

    },


	receive: () => {


        return ipcRenderer.on("download-progress", (event, args) => {


            console.log("RC",args)

        })
        // let validChannels = ["fromMain"];
        // if (validChannels.includes(channel)) {
        //     // Deliberately strip event as it includes `sender` 
        //     // ipcMain.on(channel, (event, ...args) => func(...args));
        // }
    },

	complite: () => {

        ipcRenderer.on("download-complete", (event, args) => {

            console.log("complte",args)

			const chatBox = document.getElementById("progress");
			chatBox.innerHTML ="end"

            // const progress = args

            // console.log(progress)
            // desktop: true
            // ipcRenderer.send("finish", args);

            // return  ipcRenderer.invoke("get-friends")

            // return args


        })


    //     // let validChannels = ["fromMain"];
    //     // if (validChannels.includes(channel)) {
    //     //     // Deliberately strip event as it includes `sender` 
    //     //     // ipcMain.on(channel, (event, ...args) => func(...args));
    //     // }
    },

})

window.addEventListener("DOMContentLoaded", () => {
	ipcRenderer.send("main-window-ready");
})
